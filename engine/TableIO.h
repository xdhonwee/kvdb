#ifndef _ENGINE_TABLE_IO_H_
#define _ENGINE_TABLE_IO_H_

#include "MemTable.h"

#include "BaseType.h"

#include "Configuration.h"

#include <vector>

namespace kv_engine {

class TableWriter {
public:
    TableWriter(Configuration * conf);

    TableWriter(MemTable * mem, Configuration * conf);

    ~TableWriter();

    Status Init(int level, long id);

    Status Init(MemTable * mem);

    Status Init();

    Status WriteTable();

    static void WriteTableBackgroud(MemTable * mem, Configuration * conf);

    static void SafeWriteBackground(MemTable * mem, Configuration * conf);
    
    static void TryToCompactSSTable(Configuration * conf);

    static Status CompactSSTable(int level, long & new_id, Configuration * conf);
private:
    Configuration * _conf = nullptr;

    int _fd = -1;
    
    MemTable * _mem = nullptr;

    std::vector<size_t> _index;

    size_t WriteRecord(const KeyType & key, const ValueType & value);


};

// TODO: use mmap
class TableReader {
public:
    TableReader(int level, long id, Configuration * conf);

    TableReader();

    ~TableReader();

    Status Init(int level, long id);

    Status Init();

    Status Remove();
    class Iterator {
    public:
        Iterator(){}

        Iterator(TableReader* reader);
        
        Iterator(const TableReader::Iterator & that);
        
        void FromReader(TableReader * reader);

        bool next();

        bool end();

        void ReadRecord(KeyType & key, ValueType & value);

        void ReadKey(KeyType & key);
    private:
        TableReader* _reader = nullptr;

        off_t _offset = 0;

        KeyType _key;

        ValueType _value;
    };

    Status Find(const KeyType & key, ValueType & value);

    TableReader::Iterator Begin();

    friend class TableReader::Iterator;
private:
    Configuration * _conf = nullptr;

    int _fd = -1;

    int _level = -1;

    long _id = -1;

    off_t _index_offset = -1;

    size_t _size = -1;

    int _readers = 0;

    mutex _readers_mtx;

    condition_variable _readers_cv;

    // NOT Safe !!!
    Status _ReadRecord(size_t offset, KeyType & key, ValueType & value);

    Status _ReadIndex(int n, size_t & index);

    Status _ReadKey(size_t offset, KeyType & key);
    
    Status _ReadValue(size_t offset, ValueType & value);

    Status _ReadKeyWithIndex(int index, KeyType & key, size_t & offset);

    Status _BinarySearch(const KeyType & key, ValueType & value);

};

} // namespace kv_engine
#endif // _ENGINE_TABLE_IO_H_