#ifndef _ENGINE_CONFIGURATION_H_
#define _ENGINE_CONFIGURATION_H_

#include <string>

#include <map>

#include "Util.h"

#define SET_CONF_VALUE(ss, value, variable) { \
	ss << value; 							  \
	ss >> variable;                           \
	continue;								  \
}

namespace kv_engine {
using std::string;

class TableReader;

class Meta;

class Configuration {
public:
	Configuration();
	
	~Configuration();

	string GetDataDir(int level);

	Status Init(string conf_path);

	int SECONDARY_LEVEL;

	size_t MAX_MEMTABLE_SIZE;

	int COMPACT_SIZE;
	
	string SSTABLE_NAME;
		
	string DBLOG_NAME;
	
	Meta * meta;

	string DATA_DIR;

	string SECONDARY_DATA_DIR;

	string LOG_DIR;

	std::map<long, TableReader*> TableReaderMap;
private:
	std::map<string, string> _conf_kv;
	void SetConf();
};
} // namespace kv_engine

#endif