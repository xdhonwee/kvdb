#include "KVEngine.h"

#include "MemTable.h"

#include "Configuration.h"

#include "TableIO.h"

#include "Meta.h"

#include <dirent.h>

#include <sys/stat.h>

#include <list>
namespace kv_engine {
using std::string;

KVEngine::~KVEngine() {
    Close();
}
Status KVEngine::Open(const std::string & conf_path) {
    _conf = new Configuration();
    if (_conf->Init(conf_path) != Success)
        return FileNotFound;
    return Open();
}

Status KVEngine::Open(const std::string & log_dir, const std::string & data_dir, const std::string & secondary_data_dir) {
    _conf = new Configuration();
    _conf->LOG_DIR = log_dir;
    _conf->DATA_DIR = data_dir;
    _conf->SECONDARY_DATA_DIR = secondary_data_dir;
    return Open();
}

Status KVEngine::Open() {
    INFOLOG("Opening kvdb");
    if (_conf->LOG_DIR == _conf->DATA_DIR || _conf->LOG_DIR == _conf->SECONDARY_DATA_DIR || _conf->DATA_DIR == _conf->SECONDARY_DATA_DIR) {
        ERRORLOG("log_dir, data_dir and secondary_data_dir can not be the same one.");
        return FileNotFound;
    }

    // Open data file
    DIR* _data_dir = opendir(_conf->DATA_DIR.data());
    if (_data_dir == NULL) {
        if (::mkdir(_conf->DATA_DIR.data(), 0777) != 0) {
            ERRORLOG("Can't create dir %s", _conf->DATA_DIR.data());
            return FileNotFound;
        }
    } else {
        // search data file
        dirent * entry;
        while ((entry = readdir(_data_dir)) != nullptr) {
            string file_name(entry->d_name);
            int level;
            long id;
            string file_head;
            if (SplitFileName(file_name, file_head, level, id) != Success)
                continue;

            if (file_head != _conf->SSTABLE_NAME)
                continue;
            TableReader* new_reader = new TableReader(level, id, _conf);
            if (new_reader->Init() != Success)
                return FileNotFound;
            _conf->TableReaderMap[id] = new_reader;
        }
        closedir(_data_dir);
    }
    // Open secondary data file
    DIR* _secondary_data_dir = opendir(_conf->SECONDARY_DATA_DIR.data());
    if (_secondary_data_dir == NULL) {
        if (::mkdir(_conf->SECONDARY_DATA_DIR.data(), 0777) != 0) {
            ERRORLOG("Can't create dir %s", _conf->SECONDARY_DATA_DIR.data());
            return FileNotFound;
        }
    } else {
        // search secondary data file
        dirent * entry;
        while ((entry = readdir(_secondary_data_dir)) != nullptr) {
            string file_name(entry->d_name);
            int level;
            long id;
            string file_head;
            if (SplitFileName(file_name, file_head, level, id) != Success)
                continue;
            if (file_head != _conf->SSTABLE_NAME)
                continue;
            TableReader* new_reader = new TableReader(level, id, _conf);
            if (new_reader->Init() != Success)
                return IOError;
            _conf->TableReaderMap[id] = new_reader;
        }
        closedir(_secondary_data_dir);
    }

    // recover meta 
    _conf->meta->Deserialize((_conf->DATA_DIR + "/meta").data());
    
    // log
    // create MemTbale
    DIR* _log_dir = opendir(_conf->LOG_DIR.data());
    if (_log_dir == NULL) {
        if (::mkdir(_conf->LOG_DIR.data(), 0777) != 0) {
            ERRORLOG("Can't create dir %s", _conf->LOG_DIR.data());
            return FileNotFound;
        }
        // create MemTbale
        MemTable::memTable = new MemTable(_conf);
    } else {
        // create MemTbale
        MemTable::memTable = new MemTable(_conf);
        // search log file
        dirent * entry;
        std::list<std::string> entry_list;
        while ((entry = readdir(_log_dir)) != nullptr) {
            string entry_name(entry->d_name);
            entry_list.push_back(entry_name);
        }
        closedir(_log_dir);

        for(auto iter:entry_list) {
            int level;
            long id;
            string file_head;
            if (SplitFileName(iter, file_head, level, id) != Success)
                continue;
            if (file_head != _conf->DBLOG_NAME) 
                continue;
            // recover log file
            DBLog::Recover(MemTable::memTable, id, _conf);
            if (MemTable::memTable->ApproximateMemorySize() >= _conf->MAX_MEMTABLE_SIZE)
                MemTable::memTable->SafeSetImmutable();
        }
    }

    return Success;
}

Status KVEngine::Close() {
    INFOLOG("Closing kvdb");
    MemTable * _mem = MemTable::memTable;
    MemTable::memTable = nullptr;

    TableWriter::WriteTableBackgroud(_mem, _conf);
    delete _conf;
    INFOLOG("886");
    return Success;
}
Status KVEngine::Put(const KeyType & key, const ValueType & value, const bool overwrite) {
    // Test if the memtable is writable
    if (MemTable::memTable->ApproximateMemorySize() >= _conf->MAX_MEMTABLE_SIZE)
        MemTable::memTable->SafeSetImmutable();
    
    ASSERT(MemTable::memTable->Put(key, value, overwrite));
    
    return Success;
}

Status KVEngine::Get(const KeyType & key, ValueType & value) {
    Meta * meta = _conf->meta;
    Meta::Iterator iter = meta->Begin();
    // read memTable first
    Status res = MemTable::memTable->Get(key, value);
    if (res == Success) {
        if (value.removed)
            return KeyNotFound;
        return Success;
    }

    // read ImmutMemTable
    if (MemTable::immutableTable != nullptr) {
        res = MemTable::immutableTable->Get(key, value);
        if (res == Success) {
            if (value.removed)
                return KeyNotFound;
            return Success;
        }
    }

    // read SSTables
    SSTABLE_INFO* info = nullptr;
    while((info = iter.next()) != nullptr) {
        auto find = _conf->TableReaderMap.find(info->id);
        if (find == _conf->TableReaderMap.end())
            continue;
        TableReader * reader = find->second;
        if (reader == nullptr)
            continue;
        res = reader->Find(key, value);
        if (res == Success) {
            if (value.removed)
                return KeyNotFound;
            return Success;
        }
    }

    return KeyNotFound;
}

Status KVEngine::Scan(const KeyType & start, const int record_count, ScanHandle & handle) {
    return UnknownError;
}

Status KVEngine::Delete(const KeyType & key) {
    // Test if the memtable is writable
    if (MemTable::memTable->ApproximateMemorySize() >= _conf->MAX_MEMTABLE_SIZE)
        MemTable::memTable->SafeSetImmutable();
    
    ASSERT(MemTable::memTable->Delete(key));

    return Success;
}

} // namespace kv_engine