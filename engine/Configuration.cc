#include "Configuration.h"
#include "Meta.h"
#include "TableIO.h"

#include <fstream>
#include <sstream>

namespace kv_engine {

Configuration::Configuration() {
    MAX_MEMTABLE_SIZE = 512000;
    COMPACT_SIZE = 5;
    DBLOG_NAME = "DBLOG";
    SSTABLE_NAME = "SSTABLE";
    meta = new Meta(this);
    DATA_DIR = "./data";
    LOG_DIR = "./log";
    SECONDARY_DATA_DIR = "./data2";
    SECONDARY_LEVEL = 1;
}

Configuration::~Configuration() {
    delete meta;
    for(auto iter : TableReaderMap) {
        TableReader* reader = iter.second;
        delete reader;
    }
}

string Configuration::GetDataDir(int level) {
    if (level >= SECONDARY_LEVEL)
        return SECONDARY_DATA_DIR;
    return DATA_DIR;
}

Status Configuration::Init(string conf_path) {
    std::ifstream conf_file(conf_path);
    if (!conf_file.is_open()) {
        // using the function in the wrong way
        DEBUGLOG("Can't open configuration file.");
        return FileNotFound;
    }
    else {
        // read configuration file
        char buf[1024];
        while (conf_file.getline(buf,1024)) {
            string line(buf);
            // erase ' '
            int blank = -1;
            while ((blank = line.find(' ')) != -1) {
                line = line.erase(blank,1);
            }
            // comment line
            if (line[0] == '#')
                continue;
            // split key and value
            int mid = -1;
            if ((mid = line.find_first_of('=')) != -1) {
                string key = line.substr(0,mid),
                       value = line.substr(mid + 1);
                _conf_kv.insert(std::pair<string,string>(key,value));
                INFOLOG("Add configutarion key: %s , value : %s",key.c_str(),value.c_str());
            }
        }
    }
    SetConf();
    return Success;
}

void Configuration::SetConf() {
    for(auto iter : _conf_kv) {
        std::stringstream sstream;
        if (iter.first == "DATA_DIR")
            SET_CONF_VALUE(sstream, iter.second, DATA_DIR);
        if (iter.first == "LOG_DIR")
            SET_CONF_VALUE(sstream, iter.second, LOG_DIR);
        if (iter.first == "SECONDARY_DATA_DIR")
            SET_CONF_VALUE(sstream, iter.second, SECONDARY_DATA_DIR);
        if (iter.first == "SECONDARY_LEVEL")
            SET_CONF_VALUE(sstream, iter.second, SECONDARY_LEVEL);
        if (iter.first == "MAX_MEMTABLE_SIZE")
            SET_CONF_VALUE(sstream, iter.second, MAX_MEMTABLE_SIZE);
        if (iter.first == "COMPACT_SIZE")
            SET_CONF_VALUE(sstream, iter.second, COMPACT_SIZE);
    }
}


} // namespace kv_engine

