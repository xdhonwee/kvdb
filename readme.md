# KVDB

### Software Engineering Course Project

## KV Engine

###  How to build?

```
$ cd ./engine/build
$ cmake .
$ make
```

### How to use?

```
$ ./main
```

### How to access to KVDB by java?

1. specify the location of java (be sure ${JAVA_HOME} has been set)

```
$ sh addJNIPath.sh
```

2. compile

```
$ cd ./engine/release
$ cmake .
$ make
```

3. copy the ./engine/release/libkvdb.so to /lib

```
$ sudo cp libkvdb.so /lib
```
### HOW to add KVDB to YCSB

1. be sure that the libkvdb.so is in /lib

2. put YCSB in the root directory of kvdb so that the configYCSB.sh make sense

```
$ sh configYCSB.sh
```
   
3. add the javaDriver to Maven local repository

```
$ mvn install:install-file -Dfile=./YCSB/kvdb/libs/kvdb.jar -DgroupId=org.kvdb -DartifactId=driver -Dversion=1.0.0 -Dpackaging=jar
```

4. add the following into YCSB/distribution/pom.xml: 

```
<dependency>
    <groupId>com.yahoo.ycsb</groupId>
    <artifactId>kvdb-binding</artifactId>
    <version>${project.version}</version>
</dependency>
```

5. compile
```
$ cd /xxx/YCSB
$ mvn -pl com.yahoo.ycsb:kvdb-binding -am clean package
```

6. test (you must specify the location of log and data)

```
$ cd /xxx/YCSB
$ ./bin/ycsb load kvdb -s -P workloads/workloada -p "configPath=/xxx/kvdb_config.conf"
$ ./bin/ycsb run kvdb -s -P workloads/workloada -p "configPath=/xxx/kvdb_config.conf"
```

PS: you can try the raw commands in rawConfigCommand.md replacing scripts below.
