swigAuto.sh can help you complete following commands:
$ swig -c++ -java -package org.kvdb -outdir ./ -I../engine kvdb.i
$ javac ./*.java
$ mkdir -p ./org/kvdb
$ mv ./*.class ./org/kvdb
$ jar -cvf kvdb.jar ./org
