%module kvdbAccess  

%{
    #include "KVEngine.h"
    #include<iostream>
%}

%apply (char *STRING, size_t LENGTH) { (char key_in[], size_t klen),(char value_in[], size_t vlen),(char* value_out, size_t capacity),(char* value_out, size_t value_size),(char* vptr, size_t vptr_size) }
%apply (long *LONGARRAY, size_t LENGTH) { (long* value_out, size_t capacity) }
%inline %{
class Access {
public:
	Access(){
		engine_ = new kv_engine::KVEngine();
	}
        
        void close() {
            engine_->Close();
        }

	bool open(const char* conf_path) {
            std::string conf_path_str = conf_path;
	    return !engine_->Open(conf_path_str);
    }

	bool write(char key_in[], size_t klen, char value_in[], size_t vlen) {
		kv_engine::NoCopyString key = kv_engine::NoCopyString(key_in,klen);
		kv_engine::NoCopyString value = kv_engine::NoCopyString(value_in,vlen);
		return !engine_->Put(key,value,0);

	}

	bool update(char key_in[], size_t klen, char value_in[], size_t vlen) {
		kv_engine::NoCopyString key = kv_engine::NoCopyString(key_in,klen);
		kv_engine::NoCopyString value = kv_engine::NoCopyString(value_in,vlen);
		return !engine_->Put(key,value,1);
	}

	size_t read_prepare(char key_in[], size_t klen, char* vptr, size_t vptr_size) {
		kv_engine::NoCopyString key = kv_engine::NoCopyString(key_in,klen);
		kv_engine::NoCopyString * value = new kv_engine::NoCopyString();

		bool ret = !engine_->Get(key,*value);
		size_t vlen = value->size();
		
		// map the pointer into value_out
		memset(vptr, 0, 8);
		memcpy(vptr, &value, sizeof(value));

		if (ret) 
			return vlen;
		else {
			delete value;
			return -1;
		}
	}

	size_t read_value(char* vptr, size_t vptr_size, char* value_out, size_t value_size) {
		//kv_engine::NoCopyString * value = (kv_engine::NoCopyString *) vptr;
		kv_engine::NoCopyString * value;
		memcpy(&value, vptr, sizeof(value));
		if (value->size() > value_size)
			return -1;
		memcpy(value_out, value->data(), value->size());
		size_t ret_size = value->size();
		delete value;
		return ret_size;
	}

	bool remove(char key_in[], size_t klen) {
        kv_engine::NoCopyString key = kv_engine::NoCopyString(key_in,klen);
	    return !engine_->Delete(key);
    }

protected:
	kv_engine::EngineBase* engine_;
};
%}

