#!/bin/bash
echo "begin config YCSB for kvdb"
sleep 1s
`cp -r ./YCSBpart/kvdb ./YCSB`
echo "config file of YCSB"
raw1=`grep -rn "/modules" ./YCSB/pom.xml`
pos1=${raw1%%:*}
insertPos=$((${pos1}-1))
`sed -i "${insertPos}i <module>kvdb</module>" ./YCSB/pom.xml`

raw2=`grep -rn "com.yahoo.ycsb.db.TarantoolClient" ./YCSB/bin/ycsb`
pos2=${raw2%%:*}
`sed -i "${pos2}i \"kvdb\":\"com.yahoo.ycsb.db.KVDBClient\"," ./YCSB/bin/ycsb`

raw3=`grep -rn "accumulo:com.yahoo.ycsb.db.accumulo.AccumuloClient" ./YCSB/bin/bindings.properties`
pos3=${raw3%%:*}
`sed -i "${pos3}i kvdb:com.yahoo.ycsb.db.KVDBClient" ./YCSB/bin/bindings.properties`
